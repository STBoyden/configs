#!/bin/bash

#Sets Alt_R key to the same keycode as Alt_L, allowing Alt_R being used for Mod4 in i3
xmodmap -e 'keycode 108 = Alt_L' && xset -r 108
