#!/bin/bash

DROP_ID=~/.drop-id
DROP_PID=~/.drop-pid

if [[ ! -s $DROP_ID || ! -s $DROP_PID   ]]; then
  xfce4-terminal --tab --title="General Purpose" -e tmux --tab --title="Maths" -e R --drop-down
  echo -n $(~/.config/i3/scripts/getxwinid.sh) >$DROP_ID
  echo -n $! >$DROP_PID
else
  kill $(cat $DROP_PID)
  rm $DROP_PID
fi
